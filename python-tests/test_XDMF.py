# Copyright (C) 2018 Michal Habera
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.

import pytest
import os
import numpy as np
import json
from dolfin import *
from dolfin_utils.test import skip_in_parallel, fixture, tempdir


# Meshes tested
#
# TODO: This should be elsewhere, probably as global
# fixture
def mesh_factory(mpi_comm, tdim, cell_type, n):
    if tdim == 1:
        return UnitIntervalMesh.create(n)
    elif tdim == 2:
        return UnitSquareMesh.create(mpi_comm, n, n, cell_type)
    elif tdim == 3:
        return UnitCubeMesh.create(mpi_comm, n, n, n, cell_type)


def xdmf_file_factory(mpi_comm, name):
    return XDMFFile(mpi_comm, name)


def fe_function_factory(rank, mesh, fe_family, fe_degree):
    
    if rank == 0:
        FE = FiniteElement(fe_family, mesh.ufl_cell(), fe_degree)
        expression = "x[0]"
    if rank == 1:
        FE = VectorElement(fe_family, mesh.ufl_cell(), fe_degree)
        if mesh.geometry().dim() == 1:
            expression = ("x[0]", )
        if mesh.geometry().dim() == 2:
            expression = ("x[0]", "x[1]")
        if mesh.geometry().dim() == 3:
            expression = ("x[0]", "x[1]", "x[2]")
    if rank == 2:
        FE = TensorElement(fe_family, mesh.ufl_cell(), fe_degree)
        if mesh.geometry().dim() == 1:
            expression = (("x[0]", ), )
        if mesh.geometry().dim() == 2:
            expression = (("x[0]", "x[1]"), ("x[1]", "x[0]"))
        if mesh.geometry().dim() == 3:
            expression = (("x[0]", "x[1]", "x[2]"), ("x[1]", "x[2]", "x[0]"), ("x[2]", "x[0]", "x[1]"))

    V = FunctionSpace(mesh, FE)
    u = Function(V)
    u.interpolate(Expression(expression, degree=1))

    return u


# Supported XDMF file encodings
encodings = [XDMFFile.Encoding.HDF5, XDMFFile.Encoding.ASCII]
encodings_ids = ["hdf", "ascii"]

# Data types supported in templating
data_types = [('int', int), ('size_t', int), ('double', float), ('bool', bool)]
data_types_ids = ["int", "size_t", "double", "bool"]

cell_types = [(1, CellType.Type.interval), 
              (2, CellType.Type.triangle), 
              (2, CellType.Type.quadrilateral), 
              (3, CellType.Type.tetrahedron), 
              (3, CellType.Type.hexahedron)]
cell_types_ids = ["tdim=1, interval",
                  "tdim=2, triangle",
                  "tdim=2, quadrilateral",
                  "tdim=3, tetrahedron",
                  "tdim=3, hexahedron"]

# Finite elements tested
fe_families = ["CG", "DG"]
fe_degrees = [0, 1, 2]
fe_degrees_ids = ["fe_degree=0", "fe_degree=1", "fe_degree=2"]

mesh_ns = [6]
mesh_ns_ids = ["meshsize=6"]

ranks = [0, 1, 2]
ranks_ids = ["scalar", "vector", "tensor"]


def check_encoding(encoding):
    if (not has_hdf5()) and encoding == XDMFFile.Encoding.HDF5:
        pytest.skip("HDF not supported.")

    if encoding == XDMFFile.Encoding.ASCII and MPI.comm_world.size > 1:
        pytest.xfail("ASCII IO in parallel is not implemented.")

    if (not has_hdf5_parallel()) and MPI.comm_world.size > 1:
        pytest.skip("HDF doesn't have parallel support.")


@pytest.mark.parametrize("tdim, cell_type", cell_types, ids=cell_types_ids)
@pytest.mark.parametrize("n", mesh_ns, ids=mesh_ns_ids)
@pytest.mark.parametrize("encoding", encodings, ids=encodings_ids)
@pytest.mark.parametrize("", [], ids=["comm_size={}".format(MPI.comm_world.size)])
def test_mesh_write_and_read(tempdir, tdim, cell_type, n, encoding): 
   
    if cell_type == CellType.Type.hexahedron:
        pytest.xfail("Hexahedral mesh IO unsupported.")

    check_encoding(encoding)

    filename = os.path.join(tempdir, "mesh.xdmf")
    mesh_out = mesh_factory(MPI.comm_world, tdim, cell_type, n)
    with xdmf_file_factory(mesh_out.mpi_comm(), filename) as file:
        file.write(mesh_out, encoding)

    mesh_in = Mesh(MPI.comm_world)
    with xdmf_file_factory(mesh_in.mpi_comm(), filename) as file:
        file.read(mesh_in)

    assert mesh_out.num_entities_global(0) == mesh_in.num_entities_global(0)
    dim = mesh_out.topology().dim()
    assert mesh_out.num_entities_global(dim) == mesh_in.num_entities_global(dim)


@pytest.mark.parametrize("tdim, cell_type", cell_types, ids=cell_types_ids)
@pytest.mark.parametrize("encoding", encodings, ids=encodings_ids)
@pytest.mark.parametrize("fe_degree", fe_degrees, ids=fe_degrees_ids)
@pytest.mark.parametrize("fe_family", fe_families)
@pytest.mark.parametrize("n", mesh_ns, ids=mesh_ns_ids)
@pytest.mark.parametrize("rank", ranks, ids=ranks_ids)
def test_function_write_checkpoint_and_read_checkpoint(tempdir, encoding, fe_degree, fe_family,
        n, tdim, cell_type, rank):

    if fe_degree == 0 and fe_family == "CG":
        pytest.skip("Trivial finite element.")

    if fe_family == "DG" and (cell_type == CellType.Type.quadrilateral
        or cell_type == CellType.Type.hexahedron):
        pytest.xfail("DQ element is unsupported.")
    
    check_encoding(encoding)

    filename = os.path.join(tempdir, "u1_checkpoint.xdmf")
    
    mesh = mesh_factory(MPI.comm_world, tdim, cell_type, n)    
    
    u_out = fe_function_factory(rank, mesh, fe_family, fe_degree)
    u_in = Function(u_out.function_space())

    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        file.write_checkpoint(u_out, "u_out", 0, encoding)

    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        file.read_checkpoint(u_in, "u_out", 0)

    result = u_in.vector() - u_out.vector()
    assert all([near(x, 0.0) for x in result.get_local()])
   


@pytest.mark.parametrize("encoding", encodings, ids=encodings_ids)
@pytest.mark.parametrize("cell_type", [CellType.Type.triangle], ids=["triangle"])
@pytest.mark.parametrize("n", [16], ids=["mesh_n=16"])
def test_function_timeseries_write_checkpoint_and_read_checkpoint(tempdir, encoding, cell_type, n):
 
    check_encoding(encoding)
    
    mesh = mesh_factory(MPI.comm_world, 2, cell_type, n)
    filename = os.path.join(tempdir, "u2_checkpoint.xdmf")
    FE = FiniteElement("CG", mesh.ufl_cell(), 2)
    V = FunctionSpace(mesh, FE)

    times = [0.5, 0.2, 0.1]
    u_out = [None]*len(times)
    u_in = [None]*len(times)

    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        for i, p in enumerate(times):
            u_out[i] = interpolate(Expression("x[0]*p", p=p, degree=1), V)
            file.write_checkpoint(u_out[i], "u_out", p, encoding)

    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        for i, p in enumerate(times):
            u_in[i] = Function(V)
            file.read_checkpoint(u_in[i], "u_out", i)

    for i, p in enumerate(times):
        result = u_in[i].vector() - u_out[i].vector()
        assert all([near(x, 0.0) for x in result.get_local()])

    # Test reading the last, i.e. the relative position
    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        u_in_last = Function(V)
        file.read_checkpoint(u_in_last, "u_out", -1)

    result = u_out[-1].vector() - u_in_last.vector()
    assert all([near(x, 0.0) for x in result.get_local()])


@pytest.mark.parametrize("tdim, cell_type", cell_types, ids=cell_types_ids)
@pytest.mark.parametrize("encoding", encodings, ids=encodings_ids)
@pytest.mark.parametrize("fe_degree", fe_degrees, ids=fe_degrees_ids)
@pytest.mark.parametrize("fe_family", fe_families)
@pytest.mark.parametrize("n", mesh_ns, ids=mesh_ns_ids)
@pytest.mark.parametrize("rank", ranks, ids=ranks_ids)
def test_function_write(tempdir, encoding, fe_degree, fe_family,
        n, tdim, cell_type, rank):

    if fe_degree == 0 and fe_family == "CG":
        pytest.skip("Trivial finite element.")

    if fe_family == "DG" and (cell_type == CellType.Type.quadrilateral
        or cell_type == CellType.Type.hexahedron):
        pytest.xfail("DQ element is unsupported.")
    
    check_encoding(encoding)

    filename = os.path.join(tempdir, "u1.xdmf")
    
    mesh = mesh_factory(MPI.comm_world, tdim, cell_type, n)    

    u_out = fe_function_factory(rank, mesh, fe_family, fe_degree)
    u_in = Function(u_out.function_space())
    
    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        file.write(u_out, encoding)


@pytest.mark.parametrize("tdim, cell_type", cell_types, ids=cell_types_ids)
@pytest.mark.parametrize("encoding", encodings, ids=encodings_ids)
@pytest.mark.parametrize("dtype_str, dtype", data_types, ids=data_types_ids)
@pytest.mark.parametrize("n", [8], ids=["mesh_n=8"])
@pytest.mark.parametrize("mf_dim", [0, 1, 2, 3], ids=["mfdim=0", "mfdim=1", "mfdim=2", "mfdim=3"])
def test_meshfunction_write_and_read(tempdir, encoding, tdim, cell_type, dtype_str, dtype, n, mf_dim):

    if mf_dim > tdim:
        pytest.skip("MeshFunction cannot have higher dimension than the mesh itself.")

    if cell_type == CellType.Type.quadrilateral or cell_type == CellType.Type.hexahedron:
        pytest.skip("MeshFunction on quads and hexes unsupported.")

    check_encoding(encoding)
    
    filename = os.path.join(tempdir, "mf.xdmf")
    mesh = mesh_factory(MPI.comm_world, tdim, cell_type, n)
    mf = MeshFunction(dtype_str, mesh, mesh.topology().dim())
    mf.rename("meshfunction", "meshfunction")

    for cell in cells(mesh):
        mf[cell] = dtype(cell.index())

    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        file.write(mf, encoding)

    mf_in = MeshFunction(dtype_str, mesh, mesh.topology().dim())
    with xdmf_file_factory(mesh.mpi_comm(), filename) as file:
        file.read(mf_in, "meshfunction")

    diff = 0
    for cell in cells(mesh):
        diff += (mf_in[cell] - mf[cell])
    assert diff == 0
